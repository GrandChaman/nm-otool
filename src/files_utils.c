/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   files_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/24 14:18:46 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/26 15:43:57 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

void		init_file(t_file *files)
{
	files->fd = -1;
	files->path = NULL;
	files->buf = NULL;
}

void		clean_up_file(t_file *file)
{
	close(file->fd);
	if (file->buf)
		munmap(file->buf, file->stat.st_size);
}
