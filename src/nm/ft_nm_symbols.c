/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nm_symbols.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/10 12:18:13 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/10 12:53:28 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

char					ft_nm_read_symbols_init(t_nm_otool_file *file,
	void *st_ptr, struct symtab_command **st_cmd, struct nlist_64 **nlist)
{
	if (!(*st_cmd = ft_nm_access_addr(file, st_ptr)))
		return (1);
	*nlist = ft_nm_access_addr(file, (void*)file->file.buf + file->offset
		+ ft_eness_32((*st_cmd)->symoff, file->eness));
	file->string_table = ft_nm_access_addr(file, (void*)file->file.buf
		+ file->offset + ft_eness_32((*st_cmd)->stroff, file->eness));
	if (!*nlist || !file)
	{
		ft_fprintf(STDERR_FILENO, "Corrupted file\n");
		return (1);
	}
	return (0);
}

char					ft_nm_read_symbols(t_nm_otool_file *file,
	void *st_ptr)
{
	struct symtab_command	*st_cmd;
	struct nlist_64			*nlist;
	t_btree					*tree;
	size_t					i;
	char					ret;

	i = -1;
	if (ft_nm_read_symbols_init(file, st_ptr, &st_cmd, &nlist))
		return (1);
	tree = NULL;
	ret = 0;
	while (!ret && ++i < ft_eness_32(st_cmd->nsyms, file->eness))
	{
		if (!rbt_insert_node(&tree, (void*)ft_nm_create_symbol(file, nlist),
			(t_nm_cmp*)&ft_nm_cmp_symbol_name))
			ret = 1;
		else if (!(nlist = ft_nm_access_addr(file, (void*)nlist + (file->is64
					? sizeof(struct nlist_64) : sizeof(struct nlist)))))
			ret = 1;
	}
	if (!ret)
		ret = ft_nm_display_symbols(file, tree);
	bt_destroy(&tree, &free);
	return (ret);
}

char					ft_nm_display_symbol(t_nm_otool_file *nm_file,
	t_symbol *sym)
{
	char ret;

	ret = 0;
	if (sym->mode == '-')
		return (0);
	if ((sym->addr && !sym->indirect) || sym->mode == 'a'
		|| sym->mode == 'A' || sym->mode == 't' || sym->mode == 'T')
		ret |= ft_printf("%0*llx %c %.*s", nm_file->is64 ? 16 : 8,
			sym->addr, sym->mode, ((uint64_t)ft_nm_get_max_addr(nm_file)
				- (uint64_t)sym->name), sym->name) < 0;
	else
		ret |= ft_printf("%*c %.*s", nm_file->is64 ? (16 + 2) : (8 + 2),
			sym->mode, ((uint64_t)ft_nm_get_max_addr(nm_file)
				- (uint64_t)sym->name), sym->name) < 0;
	if (sym->indirect)
		ret |= ft_printf(" (indirect for %.*s)",
			((uint64_t)ft_nm_get_max_addr(nm_file)
				- (uint64_t)sym->indirect), sym->indirect) < 0;
	ret |= ft_printf("\n") < 0;
	return (ret);
}

char					ft_nm_display_symbols(t_nm_otool_file *nm_file,
	t_btree *tree)
{
	t_btree		*save;
	t_symbol	*sym;
	char		should_break;
	char		ret;

	save = tree;
	ret = 0;
	should_break = 0;
	btree_leftmost(&tree);
	while (tree && !should_break)
	{
		sym = (t_symbol*)tree->value;
		should_break = !btree_next(&tree);
		ret |= ft_nm_display_symbol(nm_file, sym);
	}
	return (0);
}
