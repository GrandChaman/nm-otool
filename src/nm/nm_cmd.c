/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm_cmd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/23 14:20:44 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 15:47:01 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

t_copt_cmd	g_global_command = {
	.text = NULL,
	.opt = NULL,
	.cb = &ft_nm,
	.desc = NULL
};

t_copt		g_copt = {
	.cmd = (t_copt_cmd*[]){&g_global_command, NULL},
	.print = &ft_printf,
	.prgm_name = "ft_nm",
	.version = VERSION,
	.desc = NULL
};
