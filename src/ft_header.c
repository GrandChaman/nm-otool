/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_header.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 16:16:46 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 17:13:42 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

char		too_small_error(t_file *file)
{
	ft_fprintf(2, "ft_nm: %s: is too small to process\n", file->path);
	return (1);
}

char		ft_read_macho_header(t_nm_otool_file *nm_file)
{
	struct mach_header	*header;

	if (nm_file->file.stat.st_size < 0
		|| (size_t)nm_file->file.stat.st_size < sizeof(struct mach_header))
		return (too_small_error(&nm_file->file));
	header = (void*)nm_file->file.buf + nm_file->offset;
	if (!((header->magic == MH_MAGIC_64) || (header->magic == MH_CIGAM_64)
		|| (header->magic == MH_MAGIC) || (header->magic == MH_CIGAM)))
		return (1);
	nm_file->is64 = (header->magic == MH_MAGIC_64)
		|| (header->magic == MH_CIGAM_64);
	nm_file->eness = (header->magic == MH_CIGAM_64)
		|| (header->magic == MH_CIGAM);
	nm_file->ncmds = ft_eness_32(header->ncmds, nm_file->eness);
	nm_file->sizeofcmds = ft_eness_32(header->sizeofcmds, nm_file->eness);
	nm_file->cpu = ft_nm_get_arch(ft_eness_32(header->cputype, nm_file->eness),
		ft_eness_32(header->cpusubtype, nm_file->eness));
	return (0);
}

char		ft_read_fat_header(t_file *file)
{
	struct fat_header	*header;

	if (file->stat.st_size < 0
		|| (size_t)file->stat.st_size < sizeof(struct fat_header))
		return (too_small_error(file));
	header = (struct fat_header*)file->buf;
	if (!((header->magic == FAT_MAGIC_64) || (header->magic == FAT_CIGAM_64)
		|| (header->magic == FAT_MAGIC) || (header->magic == FAT_CIGAM)))
		return (1);
	return (0);
}

char		ft_read_archive_header(t_file *file)
{
	if (file->stat.st_size < (off_t)UNIX_ARCHIVE_HEADER_LEN)
		return (too_small_error(file));
	if (ft_memcmp(UNIX_ARCHIVE_HEADER, file->buf, UNIX_ARCHIVE_HEADER_LEN))
		return (1);
	return (0);
}
