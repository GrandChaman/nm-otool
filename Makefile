# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/16 13:28:08 by fle-roy           #+#    #+#              #
#    Updated: 2019/12/10 12:55:03 by fle-roy          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

vpath %.c src
vpath %.c src/nm
vpath %.c src/otool
vpath %.c tests

SRC_NM = \
			nm_cmd.c \
			ft_nm.c
SRC_OTOOL = \
			otool_cmd.c \
			ft_otool.c

SRC = \
	main.c \
	ft_nm_otool.c \
	ft_archive.c \
	ft_fat.c \
	ft_header.c \
	ft_macho.c \
	files.c \
	files_utils.c \
	ft_nm_symbols_utils.c \
	utils.c \
	errors.c \
	addr_utils.c \
	ft_sections.c \
	ft_sections_utils.c \
	ft_nm_symbols.c \
	ft_nm_sorter.c \
	ft_otool_display.c \
	ft_nm_otool_utils.c

INCLUDE= include
OBJ_DIR= obj
DEP_DIR= dep
VERSION_DEFINE := $(shell git describe master --tags)-$(shell \
	git rev-parse --short HEAD)
CFLAGS = -g3 -Wall -Wextra -Werror -I $(INCLUDE) -I ./copt/include \
	-I ./libft/include -DVERSION="\"$(VERSION_DEFINE)\""
CC = cc
LN = cc
OBJ = $(SRC:%.c=$(OBJ_DIR)/%.o)
OBJ_NM = $(SRC_NM:%.c=$(OBJ_DIR)/%.o)
OBJ_OTOOL = $(SRC_OTOOL:%.c=$(OBJ_DIR)/%.o)
DEP = $(SRC:%.c=$(DEP_DIR)/%.d)
DEP_NM = $(SRC_NM:%.c=$(DEP_DIR)/%.o)
DEP_OTOOL = $(SRC_OTOOL:%.c=$(DEP_DIR)/%.o)
ifeq ($(DEV),true)
	CFLAGS += -g3 -DDEV
endif

ifeq ($(CODE_COVERAGE),true)
	CFLAGS += -fprofile-arcs -ftest-coverage
endif
LIBFT = libft/bin/libft.a
COPT = copt/libcopt.a
NAME_NM = ft_nm
NAME_OTOOL = ft_otool
NAME_NM_UP = FT_NM
NAME_OTOOL_UP = FT_OTOOL
NAME_UP = FT_NM_OTOOL
all: $(NAME_NM) $(NAME_OTOOL)
$(LIBFT):
	@$(MAKE) -C libft
$(COPT):
	@$(MAKE) -C copt
$(OBJ_DIR)/%.o: %.c
	@printf "\033[K[$(NAME_UP)] \033[1;32mBuilding $<\033[0m\n"
	@$(CC) $(CFLAGS) -c $< -o $@
$(DEP_DIR)/%.d: %.c
	@printf "\033[K[$(NAME_UP)] \033[1;32mGenerating dependencies - $<\033[0m\n"
	@$(CC) $(CFLAGS) -MM $^ | sed -e '1s/^/$(OBJ_DIR)\//' > $@
$(NAME_NM): $(LIBFT) $(COPT) $(OBJ) $(OBJ_NM)
	@printf "\033[K[$(NAME_NM_UP)] \033[1;32mLinking...\033[0m\n"
	@$(CC) $(CFLAGS) -o $(NAME_NM) $(OBJ)  $(OBJ_NM) $(LIBFT) $(COPT)
	@printf "\033[K[$(NAME_NM_UP)] \033[1;32mDone!\033[0m\n\n"
$(NAME_OTOOL): $(LIBFT) $(COPT) $(OBJ) $(OBJ_OTOOL)
	@printf "\033[K[$(NAME_OTOOL_UP)] \033[1;32mLinking...\033[0m\n"
	@$(CC) $(CFLAGS) -o $(NAME_OTOOL) $(OBJ) $(OBJ_OTOOL) $(LIBFT) $(COPT)
	@printf "\033[K[$(NAME_OTOOL_UP)] \033[1;32mDone!\033[0m\n\n"
dclean:
	@rm -f $(DEP) $(DEP_NM) $(DEP_OTOOL)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .d!\033[0m\n"
clean: dclean
	@rm -f $(OBJ) $(OBJ_TEST) $(OBJ_NM) $(OBJ_OTOOL)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .o!\033[0m\n"
fclean: clean
	@rm -rf $(NAME_NM) $(NAME_OTOOL)
	@printf "[$(NAME_UP)] \033[1;31mCleaned execs!\033[0m\n"
re:
	@$(MAKE) -C libft re
	@$(MAKE) -C copt re
	@$(MAKE) fclean
	@$(MAKE) all
-include $(DEP)
-include $(DEP_NM)
-include $(DEP_OTOOL)
.PHONY: all clean fclean re dclean test libft libcopt
