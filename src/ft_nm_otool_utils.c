/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nm_otool_utils.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/10 12:54:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/10 12:54:45 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

char						ft_nm_otool_destroy(t_nm_otool_file *nm_file)
{
	size_t		i;

	i = 0;
	if (!nm_file)
		return (0);
	if (nm_file->segments)
		while (i < nm_file->nb_segments)
			free(nm_file->segments[i++]);
	free(nm_file->segments);
	free(nm_file);
	return (0);
}
