/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nm_sorter.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/05 16:53:26 by fle-roy           #+#    #+#             */
/*   Updated: 2019/11/05 16:53:38 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

ssize_t		ft_nm_cmp_symbol_name(t_symbol *s1, t_symbol *s2)
{
	int res;

	res = ft_strcmp(s2->name, s1->name);
	if (!res)
		res = s2->addr - s1->addr;
	return (res);
}

ssize_t		ft_nm_cmp_symbol_addr(t_symbol *s1, t_symbol *s2)
{
	if (s1->addr >= s2->addr)
		return (1);
	else
		return (-1);
}
