/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/08 15:41:36 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 17:18:42 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

uint64_t		ft_nm_arch(t_nm_otool_file *file, uint64_t value)
{
	return (file->is64 ? value : (uint32_t)value);
}

t_nm_otool_cpu	ft_nm_get_arch(cpu_type_t type, cpu_subtype_t stype)
{
	size_t	i;

	i = -1;
	while (g_cpu_map[++i].name)
		if (g_cpu_map[i].cpu_type == type
			&& (!g_cpu_map[i].cpu_subtype || g_cpu_map[i].cpu_subtype & stype))
			return (g_cpu_map[i]);
	return (g_cpu_map[i - 1]);
}

const char		*ft_nm_get_arch_name(cpu_type_t type, cpu_subtype_t stype)
{
	size_t	i;

	i = -1;
	while (g_cpu_map[++i].name)
		if (g_cpu_map[i].cpu_type == type
			&& (!g_cpu_map[i].cpu_subtype || g_cpu_map[i].cpu_subtype & stype))
			return (g_cpu_map[i].name);
	return (NULL);
}

uint32_t		ft_eness_32(uint32_t input, char rev)
{
	if (rev)
	{
		return ((input >> 24) |
			((input & 0x00FF0000) >> 8) |
			((input & 0x0000FF00) << 8) |
			(input << 24));
	}
	return (input);
}

uint64_t		ft_eness_64(uint64_t input, char rev)
{
	if (rev)
	{
		return ((((input) >> 56) & 0x00000000000000FF)
			| (((input) >> 40) & 0x000000000000FF00)
			| (((input) >> 24) & 0x0000000000FF0000)
			| (((input) >> 8) & 0x00000000FF000000)
			| (((input) << 8) & 0x000000FF00000000)
			| (((input) << 24) & 0x0000FF0000000000)
			| (((input) << 40) & 0x00FF000000000000)
			| (((input) << 56) & 0xFF00000000000000));
	}
	return (input);
}
