/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nm_symbols_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/25 13:47:10 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 15:45:14 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

char		ft_nm_get_symbol_type_routine_section(t_nm_otool_file *file_ref,
	struct nlist_64 *sym)
{
	size_t		i;
	uint8_t		sect;

	sect = sym->n_sect;
	i = -1;
	if (sect > 0 && file_ref->segments[sect - 1])
		while (g_section_map[++i].segname)
			if (!ft_strcmp(file_ref->segments[sect - 1]->segname,
				g_section_map[i].segname)
				&& !ft_strcmp(file_ref->segments[sect - 1]->sectname,
				g_section_map[i].secname))
				return (g_section_map[i].value);
	return ('s');
}

char		ft_nm_get_symbol_type_routine(t_nm_otool_file *file_ref,
	struct nlist_64 *sym)
{
	uint8_t		type;

	type = sym->n_type;
	if ((type & N_STAB))
		return ('-');
	if ((type & N_TYPE) == N_INDR)
		return ('i');
	if (((type & N_TYPE) == N_UNDF && ft_nm_arch(file_ref, sym->n_value) == 0)
		|| ((type & N_TYPE) == N_PBUD))
		return ('u');
	if ((type & N_TYPE) == N_UNDF
		&& ft_eness_64(ft_nm_arch(file_ref, sym->n_value), file_ref->eness))
		return ('c');
	if ((type & N_TYPE) == N_ABS)
		return ('a');
	if (type & N_SECT)
		return (ft_nm_get_symbol_type_routine_section(file_ref, sym));
	return ('?');
}

char		ft_nm_get_symbol_type(t_nm_otool_file *file_ref,
	struct nlist_64 *sym)
{
	char res;

	res = ft_nm_get_symbol_type_routine(file_ref, sym);
	if (ft_isalpha(res) && (sym->n_type & N_EXT))
		res = ft_toupper(res);
	return (res);
}

t_symbol	*ft_nm_create_symbol(t_nm_otool_file *file_ref,
	struct nlist_64 *sym)
{
	t_symbol		*res;

	res = ft_memalloc(sizeof(t_symbol));
	if (sym->n_value && file_ref->is64)
		res->addr = ft_eness_64(sym->n_value, file_ref->eness);
	else
		res->addr = ft_eness_32(((struct nlist*)sym)->n_value, file_ref->eness);
	if (!(res->name = ft_nm_access_addr(file_ref, (char*)file_ref->string_table
		+ ft_eness_32(sym->n_un.n_strx, file_ref->eness))))
		res->name = "bad string index";
	res->file_ref = file_ref;
	res->mode = ft_nm_get_symbol_type(file_ref, sym);
	if (res->mode == 'i' || res->mode == 'I')
		if (!(res->indirect = ft_nm_access_addr(file_ref,
			(char*)file_ref->string_table
			+ ft_eness_64(sym->n_value, file_ref->eness))))
			res->indirect = "bad string index";
	return (res);
}
