/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_macho.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/10 12:15:10 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/11 11:21:41 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

void					ft_read_load_commands_count_sections(
	t_nm_otool_file *file, struct load_command *lc, size_t *nb_sections)
{
	if (ft_eness_32(lc->cmd, file->eness) == LC_SEGMENT
		|| ft_eness_32(lc->cmd, file->eness) == LC_SEGMENT_64)
		*nb_sections += (file->is64
			? ft_eness_32(((struct segment_command_64*)lc)->nsects, file->eness)
			:
			ft_eness_32((((struct segment_command*)lc)->nsects), file->eness));
}

struct symtab_command	*ft_read_load_commands(t_nm_otool_file *nm_file)
{
	struct load_command			*lc;
	struct symtab_command		*res;
	size_t						i;
	size_t						nb_sect;

	i = -1;
	nb_sect = 0;
	res = NULL;
	lc = (void*)nm_file->file.buf + nm_file->offset + (nm_file->is64
		? sizeof(struct mach_header_64) : sizeof(struct mach_header));
	while (++i < nm_file->ncmds)
	{
		if (!ft_nm_access_addr(nm_file, lc))
		{
			ft_nm_otool_error(nm_file->file.path, "Corrupted file");
			return (NULL);
		}
		if (!res && ft_eness_32(lc->cmd, nm_file->eness) == LC_SYMTAB)
			res = (struct symtab_command*)lc;
		ft_read_load_commands_count_sections(nm_file, lc, &nb_sect);
		lc = (void*)lc + ft_eness_32(lc->cmdsize, nm_file->eness);
	}
	nm_file->nb_segments = nb_sect;
	nm_file->segments = ft_memalloc(sizeof(t_nm_otool_sect) * (nb_sect + 1));
	return (res);
}

char					ft_nm_otool_routine(t_nm_otool_file *nm_file,
	char (*f)(t_nm_otool_file*))
{
	char ret;

	ret = 0;
	if (!nm_file)
		return (1);
	ret = f(nm_file);
	ft_nm_otool_destroy(nm_file);
	return (ret);
}

t_nm_otool_file			*ft_nm_otool_init(t_file *file, uint32_t offset,
	uint32_t max)
{
	t_nm_otool_file	*nm_file;
	char			ret;

	ret = 0;
	if (!(nm_file = ft_memalloc(sizeof(t_nm_otool_file))))
		return (NULL);
	nm_file->file = *file;
	nm_file->offset = offset;
	nm_file->max_offset = (max > file->stat.st_size ? file->stat.st_size : max);
	if (nm_file->max_offset < nm_file->offset)
		ret = (ft_fprintf(STDERR_FILENO, "Corrupted file\n") && 1);
	if (!ret && (max - offset < sizeof(struct mach_header)
		|| ft_read_macho_header(nm_file)))
		ret = (ft_fprintf(STDERR_FILENO, "%s: Invalid file\n",
			file->path) && 1);
	if (!ret && max - offset < nm_file->sizeofcmds)
		ret = (ft_fprintf(STDERR_FILENO, "Corrupted file\n") && 1);
	if (ret)
	{
		ft_nm_otool_destroy(nm_file);
		return (NULL);
	}
	return (nm_file);
}
