/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   files.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/24 14:18:13 by fle-roy           #+#    #+#             */
/*   Updated: 2019/11/04 14:01:10 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

char		open_file(t_file *file, const char *path)
{
	char	ret;

	ret = 0;
	file->path = path;
	if ((file->fd = open(file->path, O_RDONLY)) < 0)
		ret = (ft_fprintf(2, "Error while openning %s\n", file->path)) && 1;
	else if ((fstat(file->fd, &file->stat)))
		ret = (ft_fprintf(2, "Error while `fstat` on %s\n", file->path)) && 1;
	if (ret)
		clean_up_file(file);
	return (ret);
}

ssize_t		read_file(t_file *file)
{
	if ((file->buf = mmap(NULL, file->stat.st_size, PROT_READ, MAP_PRIVATE,
			file->fd, 0)) == MAP_FAILED)
	{
		ft_fprintf(2, "Error while mmaping file %s\n", file->path);
		return (-1);
	}
	return (file->stat.st_size);
}
