/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_archive.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/10 12:15:15 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 18:08:49 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

void		ft_archive_parse(t_unix_archive *ar_raw,
	t_unix_archive_parsed *ar)
{
	char				tmp[17];

	ft_bzero(tmp, 17);
	ft_memcpy(tmp, ar_raw->file, UNIX_ARCHIVE_NAME_LEN);
	ar->file_name_len = ft_atoi(tmp + 3);
	ft_bzero(tmp, UNIX_ARCHIVE_NAME_LEN);
	ft_memmove(tmp, ar_raw->file_size, UNIX_ARCHIVE_SIZE_LEN);
	ar->file_size = ft_atoi(tmp);
	ft_bzero(tmp, UNIX_ARCHIVE_SIZE_LEN);
	ar->file_name = (char*)((uintptr_t)ar_raw + sizeof(t_unix_archive));
}

char		ft_archive_routine(t_file *file, char (*f)(t_nm_otool_file*),
	char is_nm)
{
	t_unix_archive			*ar_raw;
	t_unix_archive_parsed	ar;
	size_t					i;
	char					ret;

	i = UNIX_ARCHIVE_HEADER_LEN;
	ret = 0;
	while (i < (size_t)file->stat.st_size)
	{
		ar_raw = (void*)file->buf + i;
		ft_archive_parse(ar_raw, &ar);
		if (ft_memcmp("__.SYMDEF SORTED",
			ar.file_name, ar.file_name_len > 16 ? 16 : ar.file_name_len)
			&& ft_memcmp("__.SYMDEF",
				ar.file_name, ar.file_name_len > 9 ? 9 : ar.file_name_len))
		{
			ft_printf("%s%s(%.*s):\n", is_nm ? "\n" : "",
				file->path, ar.file_name_len, ar.file_name);
			ret |= ft_nm_otool_routine(ft_nm_otool_init(file,
				i + sizeof(t_unix_archive) + ar.file_name_len, i
				+ sizeof(t_unix_archive) + ar.file_name_len + ar.file_size), f);
		}
		i += ar.file_size + sizeof(t_unix_archive);
	}
	return (ret);
}
