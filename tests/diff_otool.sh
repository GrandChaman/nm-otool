#!/bin/zsh
for file in $@ ./tests/safe/* /bin/* /sbin/* /usr/bin/* /usr/sbin/* /usr/lib/* /usr/lib/*/* /usr/share/*
do
	echo "diff on $file"
	otool -t $file | grep -v "is not an object file" > /tmp/resotool 2> /tmp/errotool
	./ft_otool $file > /tmp/resftotool 2> /tmp/errftotool
	diff /tmp/resotool /tmp/resftotool
done
rm -f /tmp/resotool
rm -f /tmp/resftotool
rm -f /tmp/errotool
rm -f /tmp/errftotool
