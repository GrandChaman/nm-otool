/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   addr_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/05 17:37:59 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 16:53:34 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

void					*ft_nm_access_offset(t_nm_otool_file *nmf,
	uint32_t roffset, char check_endianess)
{
	uint64_t	coffset;
	uintptr_t	res;

	coffset = (check_endianess ? ft_eness_32(roffset, nmf->eness)
		: roffset);
	res = nmf->offset + coffset;
	if (res > nmf->max_offset)
	{
		ft_fprintf(STDERR_FILENO, "%s: corrupted file.\n", nmf->file.path);
		return (NULL);
	}
	return ((void*)nmf->file.buf + res);
}

void					*ft_nm_access_addr(t_nm_otool_file *nmf, void *addr)
{
	if ((uintptr_t)addr > (uintptr_t)ft_nm_get_max_addr(nmf)
		|| (uintptr_t)addr < (uintptr_t)ft_nm_get_base_addr(nmf))
		return (NULL);
	return (addr);
}

void					*ft_nm_get_max_addr(t_nm_otool_file *nmf)
{
	return (nmf->file.buf + nmf->max_offset);
}

void					*ft_nm_get_base_addr(t_nm_otool_file *nmf)
{
	return (nmf->file.buf + nmf->offset);
}

char					ft_nm_otool_sections_next_lc(t_nm_otool_file *nm_file,
	struct load_command **lc)
{
	if (!(*lc = ft_nm_access_addr(nm_file,
		(void*)((uintptr_t)(*lc)
			+ ft_eness_32((*lc)->cmdsize, nm_file->eness)))))
		return (1);
	return (0);
}
