/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/05 14:03:44 by fle-roy           #+#    #+#             */
/*   Updated: 2019/11/06 15:14:33 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

char		ft_nm_otool_error(const char *file, char *error)
{
	if (!file)
		ft_fprintf(2, "%s: %s\n", g_copt.prgm_name, error);
	else
		ft_fprintf(2, "%s: %s: %s\n", g_copt.prgm_name, file, error);
	return (1);
}

char		ft_otool_error_not_found(t_nm_otool_file *file,
	const char *seg, const char *sect)
{
	ft_fprintf(2, "%s: %s: Section (%s,%s) not found\n", g_copt.prgm_name,
		file->file.path, g_copt.prgm_name, seg, sect);
	return (1);
}
