/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/23 14:20:02 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 18:43:10 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

int		main(int argc, const char **argv)
{
	t_copt_parsed	res;
	char			ret;

	ret = copt_load(g_copt, &res, argc, argv);
	copt_free_parsed(&res);
	return (ret);
}

__attribute((destructor))
int fn()
{
	if (getenv("DEBUG_LEAKS"))
		while (1);
	return (0);
}
