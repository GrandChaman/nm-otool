#!/bin/zsh
for file in $@ ./tests/safe/* /bin/* /sbin/* /usr/bin/* /usr/sbin/* /usr/lib/* /usr/lib/*/* /usr/share/*
do
	echo "diff on $file"
	nm $file > /tmp/resnm 2> /tmp/errnm
	./ft_nm $file > /tmp/resftnm 2> /tmp/errftnm
	diff /tmp/resnm /tmp/resftnm
done
rm -f /tmp/resnm
rm -f /tmp/resftnm
rm -f /tmp/errnm
rm -f /tmp/errftnm
