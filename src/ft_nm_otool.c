/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nm_otool.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 11:19:06 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/11 11:52:40 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

struct symtab_command		*ft_nm_otool_load(t_nm_otool_file *nm_file,
	char is_nm)
{
	struct symtab_command *tmp;

	if (!(tmp = ft_read_load_commands(nm_file)))
	{
		if (is_nm)
			ft_fprintf(STDERR_FILENO, "%s: no symbols.\n", nm_file->file.path);
		else
			ft_printf("Contents of (__TEXT,__text) section\n");
		return (NULL);
	}
	return (tmp);
}

char						ft_nm_callback(t_nm_otool_file *nm_file)
{
	struct symtab_command *tmp;

	if (!(tmp = ft_nm_otool_load(nm_file, 1)))
		return (0);
	if (ft_map_sections(nm_file))
		return (1);
	return (ft_nm_read_symbols(nm_file, tmp));
}

char						ft_otool_callback(t_nm_otool_file *nm_file)
{
	if (!ft_nm_otool_load(nm_file, 0))
		return (0);
	if (ft_map_sections(nm_file))
		return (1);
	return (ft_display_sections(nm_file, "__TEXT", "__text"));
}

char						ft_nm_routine_selector(t_file *file, char is_nm,
	char print_fname)
{
	char				(*f)(t_nm_otool_file*);
	t_nm_otool_file		*nm_file;

	f = (is_nm ? ft_nm_callback : ft_otool_callback);
	if (print_fname && is_nm)
		if (ft_printf("%s%s:\n", (is_nm ? "\n" : ""), file->path) < 0)
			return (1);
	if (!ft_read_fat_header(file))
		return (ft_fat_routine(file, is_nm, f));
	if (!ft_read_archive_header(file))
	{
		if (!is_nm && ft_printf("Archive : %s\n", file->path) < 0)
			return (1);
		return (ft_archive_routine(file, f, is_nm));
	}
	nm_file = ft_nm_otool_init(file, 0, file->stat.st_size);
	if (nm_file && !is_nm)
		if (ft_printf("%s%s:\n", (is_nm ? "\n" : ""), file->path) < 0)
			return (1);
	return (ft_nm_otool_routine(nm_file, f));
}

char						ft_nm_otool_routine_selector(t_copt_parsed *res,
	char is_nm)
{
	t_file		*files;
	size_t		i;
	char		ret;

	i = 0;
	ret = 0;
	if (!res->params)
		return (ft_nm_otool_error("none", "No file passed as argument"));
	files = ft_memalloc(sizeof(t_nm_otool_file) * res->params->argc);
	while (i < res->params->argc)
	{
		if (is_nm && res->params->argc > 1)
			ft_printf("\n%s:\n", res->params->argv[i]);
		if (open_file(&(files[i]), res->params->argv[i])
			|| read_file(&files[i]) < 0
			|| ft_nm_routine_selector(&files[i], is_nm, res->params->argc > 2))
			ret = 1;
		if (munmap(files[i].buf, files[i].stat.st_size) < 0)
			return (ft_fprintf(STDERR_FILENO, "munmap failed\n") && 1);
		i++;
	}
	return ((char)ft_memdelf((void**)&files, (void*)((long)ret)));
}
