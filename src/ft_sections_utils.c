/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sections_utils.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/09 17:07:34 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 17:07:36 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

char					ft_display_section(t_nm_otool_file *file, size_t idx)
{
	size_t				i;

	i = 0;
	ft_printf("Contents of (%s,%s) section\n",
		file->segments[idx]->segname, file->segments[idx]->sectname);
	return (ft_otool_print_section(file, idx));
}

char					ft_display_sections(t_nm_otool_file *file,
	const char *seg_name, const char *sect_name)
{
	size_t	i;

	i = -1;
	while (file->segments[++i])
		if (!ft_strcmp(file->segments[i]->segname, seg_name)
			&& !ft_strcmp(file->segments[i]->sectname,
				sect_name))
		{
			if (!ft_display_section(file, i))
				return (0);
			else
				return (1);
		}
	return (ft_otool_error_not_found(file, seg_name, sect_name));
}
