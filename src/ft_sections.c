/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sections.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/10 12:17:43 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 17:07:32 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

t_nm_otool_sect			*ft_create_sect_32(t_nm_otool_file *nm_file,
	struct segment_command *seg, struct section *sect)
{
	t_nm_otool_sect	*res;

	res = ft_memalloc(sizeof(t_nm_otool_sect));
	if (!res)
		return (NULL);
	res->offset = ft_eness_32(sect->offset, nm_file->eness);
	res->relative_offset = ft_eness_32(sect->offset, nm_file->eness)
		- ft_eness_32(seg->fileoff, nm_file->eness);
	res->addr = ft_eness_32(sect->addr, nm_file->eness);
	res->sectname = sect->sectname;
	res->segname = sect->segname;
	res->size = ft_eness_32(sect->size, nm_file->eness);
	res->vmaddr = ft_eness_32(seg->vmaddr, nm_file->eness);
	return (res);
}

t_nm_otool_sect			*ft_create_sect_64(t_nm_otool_file *nm_file,
	struct segment_command_64 *seg, struct section_64 *sect)
{
	t_nm_otool_sect	*res;

	res = ft_memalloc(sizeof(t_nm_otool_sect));
	if (!res)
		return (NULL);
	res->offset = ft_eness_32(sect->offset, nm_file->eness);
	res->relative_offset = ft_eness_32(sect->offset, nm_file->eness)
		- ft_eness_64(seg->fileoff, nm_file->eness);
	res->addr = ft_eness_64(sect->addr, nm_file->eness);
	res->sectname = sect->sectname;
	res->segname = sect->segname;
	res->size = ft_eness_64(sect->size, nm_file->eness);
	res->vmaddr = ft_eness_64(seg->vmaddr, nm_file->eness);
	return (res);
}

char					ft_map_sections_routine(t_nm_otool_file *nm_file,
	struct segment_command **lc, size_t *cursor)
{
	struct section				*sc;
	size_t						j;

	if (ft_eness_32((*lc)->cmd, nm_file->eness) == LC_SEGMENT)
	{
		if (!(sc = ft_nm_access_addr(nm_file,
			((void*)(*lc) + sizeof(struct segment_command)))))
			return (1);
		j = -1;
		while (++j < ft_eness_32((*lc)->nsects, nm_file->eness))
		{
			if (!(nm_file->segments[(*cursor)++] = ft_create_sect_32(nm_file,
				*lc, sc)))
				return (1);
			if (!(sc = ft_nm_access_addr(nm_file,
				((void*)sc + sizeof(struct section)))))
				return (1);
		}
	}
	if (ft_nm_otool_sections_next_lc(nm_file, (struct load_command**)lc))
		return (1);
	return (0);
}

char					ft_map_sections_64_routine(t_nm_otool_file *nm_file,
	struct segment_command_64 **lc, size_t *cursor)
{
	struct section_64			*sc;
	size_t						j;

	if (ft_eness_32((*lc)->cmd, nm_file->eness) == LC_SEGMENT_64)
	{
		if (!(sc = (struct section_64*)ft_nm_access_addr(nm_file,
			((void*)((uintptr_t)(*lc) + sizeof(struct segment_command_64))))))
			return (1);
		j = -1;
		while (++j < ft_eness_32((*lc)->nsects, nm_file->eness))
		{
			if (!(nm_file->segments[(*cursor)++] = ft_create_sect_64(nm_file,
				*lc, sc)))
				return (1);
			if (!(sc = ft_nm_access_addr(nm_file,
				((void*)sc + sizeof(struct section_64)))))
				return (1);
		}
	}
	if (ft_nm_otool_sections_next_lc(nm_file, (struct load_command**)lc))
		return (1);
	return (0);
}

char					ft_map_sections(t_nm_otool_file *nm_file)
{
	struct segment_command		*lc;
	struct segment_command_64	*lc64;
	size_t						i;
	size_t						ii;

	i = -1;
	ii = 0;
	lc = ft_nm_access_offset(nm_file, sizeof(struct mach_header), 0);
	lc64 = ft_nm_access_offset(nm_file, sizeof(struct mach_header_64), 0);
	while (++i < nm_file->ncmds)
		if (!nm_file->is64 && ft_map_sections_routine(nm_file, &lc, &ii))
			return (1);
		else if (nm_file->is64 &&
			ft_map_sections_64_routine(nm_file, &lc64, &ii))
			return (1);
	return (0);
}
