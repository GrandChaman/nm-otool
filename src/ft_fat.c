/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fat.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/10 12:15:12 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/10 12:09:31 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

char		ft_fat_run_nm_routine(t_file *file, struct fat_arch farch,
	char eness, char (*f)(t_nm_otool_file*))
{
	return (ft_nm_otool_routine(ft_nm_otool_init(file,
				ft_eness_32(farch.offset, eness),
				ft_eness_32(farch.offset, eness)
				+ ft_eness_32(farch.size, eness)), f));
}

char		ft_nm_fat_display_arch(t_file *file, char is_nm, char eness,
	struct fat_arch farch)
{
	if (ft_printf("%s%s (%sarchitecture %s):\n", is_nm ? "\n" : "",
				file->path, (is_nm ? "for " : ""),
				ft_nm_get_arch_name(ft_eness_32(farch.cputype, eness),
				ft_eness_32(farch.cpusubtype, eness))) < 0)
		return (1);
	return (0);
}

char		ft_fat_routine(t_file *file, char is_nm,
	char (*f)(t_nm_otool_file*))
{
	struct fat_header	*fheader;
	struct fat_arch		*farch;
	size_t				i;
	char				ret;
	char				eness;

	i = -1;
	ret = 0;
	fheader = (struct fat_header*)file->buf;
	eness = fheader->magic & FAT_CIGAM_64 || fheader->magic & FAT_CIGAM;
	farch = (void*)fheader + sizeof(struct fat_header);
	while (++i < ft_eness_32(fheader->nfat_arch, eness))
		if (ft_eness_32(farch[i].cputype, eness) == CURRENT_CPU
			&& ft_eness_32(farch[i].cpusubtype, eness) & CURRENT_SUBCPU)
		{
			return ((!is_nm && ft_printf("%s:\n", file->path) < 0)
				|| ft_fat_run_nm_routine(file, farch[i], eness, f));
		}
	i = -1;
	while (++i < ft_eness_32(fheader->nfat_arch, eness))
	{
		ret |= ft_nm_fat_display_arch(file, is_nm, eness, farch[i]);
		ret |= ft_fat_run_nm_routine(file, farch[i], eness, f);
	}
	return (ret);
}
