#!/bin/zsh
for file in ./tests/safe/*
do
	echo "diff on $file"
	nm $file > resnm 2> errnm
	./ft_nm $file > resftnm 2> errftnm
	diff resnm resftnm
done
