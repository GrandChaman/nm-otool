/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_otool_display.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 14:58:10 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/09 16:52:40 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_nm.h"

/*
** HEX_DUMP_WIDTH : 16 * 2 * 16 + 1
*/

#define HEX_DUMP_WIDTH	49
#define HEX_CHARSET		"0123456789abcdef"

uint8_t		ft_otool_to_hex_std(char *str, uint8_t nb, uint8_t off)
{
	(void)off;
	str[0] = HEX_CHARSET[(nb / 16) % 16];
	str[1] = HEX_CHARSET[nb % 16];
	str[2] = ' ';
	return (3);
}

uint8_t		ft_otool_to_hex_ppc(char *str, uint8_t nb, uint8_t off)
{
	str[0] = HEX_CHARSET[(nb / 16) % 16];
	str[1] = HEX_CHARSET[nb % 16];
	if (off && !((off + 1) % 4))
	{
		str[2] = ' ';
		return (3);
	}
	return (2);
}

char		ft_otool_print_section(t_nm_otool_file *file, size_t idx)
{
	size_t				i;
	uint8_t				ii;
	uint8_t				off;
	char				data[HEX_DUMP_WIDTH];
	unsigned char		*tmp;

	i = 0;
	while (i < file->segments[idx]->size)
	{
		ft_bzero(data, HEX_DUMP_WIDTH);
		ii = 0;
		off = 0;
		while (i + ii < file->segments[idx]->size && ii < 16)
		{
			if (!(tmp = ft_nm_access_offset(file,
				file->segments[idx]->offset + i + ii, 0)))
				return (1);
			off += file->cpu.otool_encode(data + off, *tmp, ii++);
		}
		ft_printf("%0*llx\t%.48s\n", (file->is64 ? 16 : 8),
			file->segments[idx]->vmaddr
			+ file->segments[idx]->relative_offset + i, data);
		i += 16;
	}
	return (0);
}
