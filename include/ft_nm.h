/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nm.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/23 14:23:45 by fle-roy           #+#    #+#             */
/*   Updated: 2019/12/10 11:58:41 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_NM_H
# define FT_NM_H
# include "copt.h"
# include "libft.h"
# include <sys/stat.h>
# include <unistd.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/fat.h>
# include <mach-o/arch.h>
# include <fcntl.h>
# include <sys/mman.h>
# define PAGE_TO_READ	4
# define NB_BYTE_TO_MMAP 16384
# define CURRENT_CPU CPU_TYPE_X86_64
# define CURRENT_SUBCPU CPU_SUBTYPE_I386_ALL
# define UNIX_ARCHIVE_HEADER "!<arch>\x0A"
# define UNIX_ARCHIVE_HEADER_LEN 8
# define UNIX_ARCHIVE_NAME_LEN 16
# define UNIX_ARCHIVE_SIZE_LEN 10
# ifndef VERSION
#  define VERSION "noversion"
# endif

/*
** NB_BYTE_TO_MMAP : PAGE_TO_READ * PAGE_SIZE)
*/

typedef ssize_t			t_nm_cmp(void *p1, void *p2);
typedef struct			s_section_map
{
	const char			*segname;
	const char			*secname;
	const char			value;
}						t_section_map;

typedef struct			s_cpu_map
{
	cpu_type_t			cpu_type;
	cpu_subtype_t		cpu_subtype;
	const char			*name;
	uint8_t				(*otool_encode)(char *, uint8_t, uint8_t);
}						t_nm_otool_cpu;

typedef struct			s_file
{
	struct stat			stat;
	int					fd;
	const char			*path;
	unsigned char		*buf;
}						t_file;

typedef struct			s_nm_otool_sect
{
	char				*segname;
	char				*sectname;
	size_t				vmaddr;
	size_t				size;
	size_t				offset;
	size_t				relative_offset;
	size_t				addr;
}						t_nm_otool_sect;
typedef struct			s_nm_otool_file
{
	t_file				file;
	char				is64: 1;
	char				eness: 1;
	uint32_t			ncmds;
	uint32_t			sizeofcmds;
	uint32_t			offset;
	uint32_t			max_offset;
	void				*string_table;
	t_nm_otool_sect		**segments;
	size_t				nb_segments;
	t_nm_otool_cpu		cpu;
}						t_nm_otool_file;

typedef struct			s_symbol
{
	t_nm_otool_file		*file_ref;
	uint64_t			addr;
	char				mode;
	char				*name;
	char				*indirect;
}						t_symbol;

static t_section_map	g_section_map[] = {
	{"__TEXT", "__text", 't'},
	{"__DATA", "__data", 'd'},
	{"__DATA", "__bss", 'b'},
	{NULL, NULL, 0}
};

typedef struct			s_unix_archive
{
	char				file[UNIX_ARCHIVE_NAME_LEN];
	char				date[12];
	char				owner_id[6];
	char				group_id[6];
	char				file_mode[8];
	char				file_size[UNIX_ARCHIVE_SIZE_LEN];
	char				ec[2];
}						t_unix_archive;

typedef struct			s_unix_archive_parsed
{
	char				*file_name;
	size_t				file_name_len;
	size_t				file_size;
}						t_unix_archive_parsed;

uint8_t					ft_otool_to_hex_std(char *str, uint8_t nb, uint8_t off);
uint8_t					ft_otool_to_hex_ppc(char *str, uint8_t nb, uint8_t off);

static t_nm_otool_cpu	g_cpu_map[] =
{
	{CPU_TYPE_X86_64, CPU_SUBTYPE_X86_64_ALL, "x86_64", &ft_otool_to_hex_std},
	{CPU_TYPE_I386, CPU_SUBTYPE_I386_ALL, "i386", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_ALL, "arm", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM64, CPU_SUBTYPE_ARM64_ALL, "arm64", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM64, CPU_SUBTYPE_ARM64E, "arm64e", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM64_32, CPU_SUBTYPE_ARM64_32_V8, "arm64_32",
		&ft_otool_to_hex_std},
	{CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_ALL, "ppc", &ft_otool_to_hex_ppc},
	{CPU_TYPE_MC680x0, CPU_SUBTYPE_MC680x0_ALL, "m68k", &ft_otool_to_hex_std},
	{CPU_TYPE_HPPA, CPU_SUBTYPE_HPPA_ALL, "hppa", &ft_otool_to_hex_std},
	{CPU_TYPE_I860, CPU_SUBTYPE_I860_ALL, "i860", &ft_otool_to_hex_std},
	{CPU_TYPE_MC88000, CPU_SUBTYPE_MC88000_ALL, "m88k", &ft_otool_to_hex_std},
	{CPU_TYPE_SPARC, CPU_SUBTYPE_SPARC_ALL, "sparc", &ft_otool_to_hex_std},
	{CPU_TYPE_I386, CPU_SUBTYPE_486, "i486", &ft_otool_to_hex_std},
	{CPU_TYPE_I386, CPU_SUBTYPE_486SX, "i486SX", &ft_otool_to_hex_std},
	{CPU_TYPE_I386, CPU_SUBTYPE_PENT, "pentium", &ft_otool_to_hex_std},
	{CPU_TYPE_I386, CPU_SUBTYPE_586, "i586", &ft_otool_to_hex_std},
	{CPU_TYPE_I386, CPU_SUBTYPE_PENTPRO, "pentpro", &ft_otool_to_hex_std},
	{CPU_TYPE_I386, CPU_SUBTYPE_PENTPRO, "i686", &ft_otool_to_hex_std},
	{CPU_TYPE_I386, CPU_SUBTYPE_PENTII_M3, "pentIIm3", &ft_otool_to_hex_std},
	{CPU_TYPE_I386, CPU_SUBTYPE_PENTII_M5, "pentIIm5", &ft_otool_to_hex_std},
	{CPU_TYPE_I386, CPU_SUBTYPE_PENTIUM_4, "pentium4", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V4T, "armv4t", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V5TEJ, "armv5", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_XSCALE, "xscale", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V6, "armv6", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V6M, "armv6m", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V7, "armv7", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V7F, "armv7f", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V7S, "armv7s", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V7K, "armv7k", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V7M, "armv7m", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V7EM, "armv7em", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V8, "armv8", &ft_otool_to_hex_std},
	{CPU_TYPE_ARM64, CPU_SUBTYPE_ARM64_V8, "arm64", &ft_otool_to_hex_std},
	{CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_601, "ppc601", &ft_otool_to_hex_ppc},
	{CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_603, "ppc603", &ft_otool_to_hex_ppc},
	{CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_604, "ppc604", &ft_otool_to_hex_ppc},
	{CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_604e, "ppc604e",
		&ft_otool_to_hex_ppc},
	{CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_750, "ppc750", &ft_otool_to_hex_ppc},
	{CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_7400, "ppc7400",
		&ft_otool_to_hex_ppc},
	{CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_7450, "ppc7450",
		&ft_otool_to_hex_ppc},
	{CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_970, "ppc970", &ft_otool_to_hex_ppc},
	{CPU_TYPE_MC680x0, CPU_SUBTYPE_MC68030_ONLY, "m68030",
		&ft_otool_to_hex_std},
	{CPU_TYPE_MC680x0, CPU_SUBTYPE_MC68040, "m68040", &ft_otool_to_hex_std},
	{CPU_TYPE_HPPA, CPU_SUBTYPE_HPPA_7100LC, "hppa7100LC",
		&ft_otool_to_hex_std},
	{CPU_TYPE_ANY, CPU_SUBTYPE_MULTIPLE, "Unknown", &ft_otool_to_hex_std},
	{0, 0, NULL, NULL}
};

t_copt					g_copt;
char					ft_nm_otool_routine_selector(t_copt_parsed *res,
	char is_nm);
char					ft_nm_routine_selector(t_file *file, char is_nm,
	char print_fname);
char					ft_otool(t_copt_parsed *res);
char					ft_nm(t_copt_parsed *res);
void					init_file(t_file *files);
char					open_file(t_file *file, const char *path);
ssize_t					read_file(t_file *file);
unsigned char			*read_at(t_file *files, size_t offset);
void					clean_up_file(t_file *files);
t_symbol				*ft_nm_create_symbol(t_nm_otool_file *file_ref,
	struct nlist_64 *sym);
ssize_t					ft_nm_cmp_symbol_name(t_symbol *s1, t_symbol *s2);
ssize_t					ft_nm_cmp_symbol_addr(t_symbol *s1, t_symbol *s2);
char					ft_nm_display_symbols(t_nm_otool_file *nm_file,
	t_btree *tree);
uint64_t				ft_nm_arch(t_nm_otool_file *file, uint64_t value);
char					ft_read_fat_header(t_file *nm_file);
char					ft_read_macho_header(t_nm_otool_file *nm_file);
t_nm_otool_cpu			ft_nm_get_arch(cpu_type_t type, cpu_subtype_t stype);
const char				*ft_nm_get_arch_name(cpu_type_t type,
	cpu_subtype_t stype);
uint64_t				ft_eness_64(uint64_t input, char rev);
uint32_t				ft_eness_32(uint32_t input, char rev);
char					ft_read_archive_header(t_file *file);
char					ft_nm_display_symbols(t_nm_otool_file *nm_file,
	t_btree *tree);
char					ft_nm_read_symbols(t_nm_otool_file *nm_file,
	void *st_ptr);
char					ft_map_sections(t_nm_otool_file *nm_file);
struct symtab_command	*ft_read_load_commands(t_nm_otool_file *nm_file);
t_nm_otool_file			*ft_nm_otool_init(t_file *file, uint32_t offset,
	uint32_t max);
char					ft_nm_otool_routine(t_nm_otool_file *nm_file,
	char (*f)(t_nm_otool_file*));
char					ft_fat_routine(t_file *file, char is_nm,
	char (*f)(t_nm_otool_file*));
char					ft_archive_routine(t_file *file,
	char (*f)(t_nm_otool_file*), char is_nm);
void					*ft_nm_access_offset(t_nm_otool_file *nmf,
	uint32_t roffset, char check_endianess);
void					*ft_nm_access_addr(t_nm_otool_file *nmf, void *addr);
void					*ft_nm_get_max_addr(t_nm_otool_file *nmf);
void					*ft_nm_get_base_addr(t_nm_otool_file *nmf);
char					ft_nm_otool_error(const char *file, char *error);
char					ft_nm_otool_sections_next_lc(t_nm_otool_file *nm_file,
	struct load_command **lc);
char					ft_display_sections(t_nm_otool_file *file,
	const char *seg_name, const char *sect_name);
char					ft_otool_print_section(t_nm_otool_file *file,
	size_t idx);
char					ft_otool_error_not_found(t_nm_otool_file *file,
	const char *seg, const char *sect);
t_btree					*bt_replace_reference_in_parent(t_btree *node,
	t_btree *ref);
void					rbt_insert_node_uncle_and_red(t_btree *uncle,
	t_btree *cursor);
void					rbt_insert_node_left_parent_line(t_btree *cursor);
void					rbt_insert_node_right_parent_line(t_btree *cursor);
void					rbt_apply_rule(t_btree *cursor);
char					ft_nm_otool_destroy(t_nm_otool_file *nm_file);

#endif
